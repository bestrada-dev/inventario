package com.inventario;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;





@SpringBootApplication
(scanBasePackages={"com.inventario"})
public class InventarioBackendApplication {



	public static void main(String[] args) {
		SpringApplication.run(InventarioBackendApplication.class, args);
	}
}
