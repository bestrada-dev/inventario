package com.inventario.dao;

import java.util.List;

import com.inventario.model.TajTarjeta;

public interface TajTarjetaDao {
	List<TajTarjeta> findAll();
}
