package com.inventario.dao;

import com.inventario.model.PrvProveedor;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository

public interface PrvProveedorDao extends CrudRepository<PrvProveedor, String> {
    List<PrvProveedor> findAll();
}
