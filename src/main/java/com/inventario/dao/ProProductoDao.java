package com.inventario.dao;


import com.inventario.model.ProProducto;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface ProProductoDao{
    List<ProProducto> findAll();
    void crearProducto(ProProducto producto);
    public ProProducto getProducto(String proCodigo);
    void borrarProducto(String proCodigo);
}
