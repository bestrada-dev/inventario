package com.inventario.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.inventario.dao.TajTarjetaDao;
import com.inventario.model.TajTarjeta;

@Repository
public class TajTarjetaDaoImpl implements TajTarjetaDao {

	@PersistenceContext	
	private EntityManager entityManager;	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TajTarjeta> findAll() {
		String hql = "FROM TajTarjeta as t ORDER BY t.tajNumero ASC";
		return (List<TajTarjeta>) entityManager.createQuery(hql).getResultList();
	}

}
