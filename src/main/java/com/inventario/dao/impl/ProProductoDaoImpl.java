package com.inventario.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.inventario.dao.ProProductoDao;
import com.inventario.model.ProProducto;
@Transactional
@Repository
public class ProProductoDaoImpl implements ProProductoDao {
	@PersistenceContext	
	private EntityManager entityManager;	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<ProProducto> findAll() {
		String hql = "FROM ProProducto as p ORDER BY p.proCodigo DESC";
		return (List<ProProducto>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public void crearProducto(ProProducto producto) {
		// TODO Auto-generated method stub
		entityManager.persist(producto);
	}
	
	@Override
	public ProProducto getProducto(String proCodigo) {
		return entityManager.find(ProProducto.class, proCodigo);
	}

	@Override
	public void borrarProducto(String proCodigo) {
		// TODO Auto-generated method stub
		entityManager.remove(getProducto(proCodigo));
	}
	
	
}
