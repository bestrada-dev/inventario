package com.inventario.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inventario.service.impl.TajTarjetaServiceImpl;

@RestController
@RequestMapping("api/tarjetas/")
public class TajTarjetaController {
	
	@Autowired
	private TajTarjetaServiceImpl tajTarjetaServiceImpl;

	@RequestMapping(value = "all", method = RequestMethod.GET, produces = "application/json")
	public String prueba() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(tajTarjetaServiceImpl.findAll());
	
	}
	
	@RequestMapping(value = "lista", method = RequestMethod.GET, produces = "application/json")
	public String lista() throws JsonProcessingException {
		return "Ejemplo de lista de tarjetas editado 01";
	
	}
	
}
