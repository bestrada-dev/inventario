package com.inventario.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inventario.model.ProProducto;
import com.inventario.service.ProProductoService;

@RestController
@RequestMapping("/api/productos")
public class ProProductoController {
	@Autowired
	private ProProductoService proProductoService;
	
	
	
	
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
	public /*List<ProProducto>*/ String findAllPro() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		
		//return proProductoService.findAll();
		
		return mapper.writeValueAsString(proProductoService.findAll());
	}
	
	@RequestMapping(value = "crear", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Void> crearProducto(@RequestBody ProProducto producto, UriComponentsBuilder builder) {
        System.out.println("Producto: "+producto.getProCodigo());
		boolean flag = proProductoService.crearProducto(producto);
        if (flag == false) {
        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "borrar", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<Void> borrarProducto(@RequestParam("prvCodigo") String prvCodigo) {
		System.out.println("Producto a borrar: "+prvCodigo);
		proProductoService.borrarProducto(prvCodigo);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	
	@RequestMapping(value = "prueba", method = RequestMethod.GET, produces = "application/json")
	public String prueba() {
	
		return "Request GET de prueba";
	}

	
}
