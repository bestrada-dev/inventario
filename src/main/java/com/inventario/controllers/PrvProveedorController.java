package com.inventario.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.inventario.model.PrvProveedor;
import com.inventario.service.PrvProveedorService;



@RestController
@RequestMapping("/api/proveedores")
public class PrvProveedorController {
	@Autowired
	private PrvProveedorService prvProveedorService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
	public List<PrvProveedor> findAllPrv() {
	 
		return prvProveedorService.findAll();
	}
	
	

}
