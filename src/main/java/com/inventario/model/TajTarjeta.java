package com.inventario.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="TAJ_TARJETA")
public class TajTarjeta implements Serializable{
	private static final long serialVersionUID = 1L;
    @Id
    private Long tajId;
    
    private int tajUserId;
    
    private String tajNumero;
    
    private int tajEstado;
    
    private int tajMora;
    
    private String tajTipo;
    
    private int tajMontoAprobado;
    
    private int tajSaldo;

	public Long getTajId() {
		return tajId;
	}

	public void setTajId(Long tajId) {
		this.tajId = tajId;
	}

	public int getTajUserId() {
		return tajUserId;
	}

	public void setTajUserId(int tajUserId) {
		this.tajUserId = tajUserId;
	}

	public String getTajNumero() {
		return tajNumero;
	}

	public void setTajNumero(String tajNumero) {
		this.tajNumero = tajNumero;
	}

	public int getTajEstado() {
		return tajEstado;
	}

	public void setTajEstado(int tajEstado) {
		this.tajEstado = tajEstado;
	}

	public int getTajMora() {
		return tajMora;
	}

	public void setTajMora(int tajMora) {
		this.tajMora = tajMora;
	}

	public String getTajTipo() {
		return tajTipo;
	}

	public void setTajTipo(String tajTipo) {
		this.tajTipo = tajTipo;
	}

	public int getTajMontoAprobado() {
		return tajMontoAprobado;
	}

	public void setTajMontoAprobado(int tajMontoAprobado) {
		this.tajMontoAprobado = tajMontoAprobado;
	}

	public int getTajSaldo() {
		return tajSaldo;
	}

	public void setTajSaldo(int tajSaldo) {
		this.tajSaldo = tajSaldo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


    
    
}



