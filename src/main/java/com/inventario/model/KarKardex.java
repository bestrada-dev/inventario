package com.inventario.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "KAR_KARDEX")
public class KarKardex {
    @Id
    @SequenceGenerator(name="SEQUENCE_KARDEX", sequenceName="SEQ_KARDEX", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQUENCE_KARDEX")

	private Long karId;
    
	private String karTipo;
	private Long karCantidad;
	private Date karFechaOperacion;

	private Long karCosto;
	

}
