package com.inventario.model;
import com.fasterxml.jackson.annotation.JsonIgnore;


import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "PRO_PRODUCTO")
public class ProProducto implements Serializable{
	private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private String proCodigo;
    
    private String proNombre;
    private String proDescripcion;
    
    private Long proPrecioVenta;
    private Long proCostoVenta;
    private Long proCostoArticulo;
    

    private String proCodigoPrv;
	

    public String getProCodigo() {
		return proCodigo;
	}
	public void setProCodigo(String proCodigo) {
		this.proCodigo = proCodigo;
	}
	public String getProNombre() {
		return proNombre;
	}
	public void setProNombre(String proNombre) {
		this.proNombre = proNombre;
	}
	public String getProDescripcion() {
		return proDescripcion;
	}
	public void setProDescripcion(String proDescripcion) {
		this.proDescripcion = proDescripcion;
	}
	public Long getProPrecioVenta() {
		return proPrecioVenta;
	}
	public void setProPrecioVenta(Long proPrecioVenta) {
		this.proPrecioVenta = proPrecioVenta;
	}
	public Long getProCostoVenta() {
		return proCostoVenta;
	}
	public void setProCostoVenta(Long proCostoVenta) {
		this.proCostoVenta = proCostoVenta;
	}
	public Long getProCostoArticulo() {
		return proCostoArticulo;
	}
	public void setProCostoArticulo(Long proCostoArticulo) {
		this.proCostoArticulo = proCostoArticulo;
	}
	public String getProCodigoPrv() {
		return proCodigoPrv;
	}
	public void setProCodigoPrv(String proCodigoPrv) {
		this.proCodigoPrv = proCodigoPrv;
	}


    
    
    

}
