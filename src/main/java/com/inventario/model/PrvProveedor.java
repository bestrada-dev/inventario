package com.inventario.model;

import com.fasterxml.jackson.annotation.JsonIgnore;


import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "PRV_PROVEEDOR")
public class PrvProveedor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String prvCodigo;
	private String prvNombre;
	

	
    public String getPrvCodigo() {
		return prvCodigo;
	}
	public void setPrvCodigo(String prvCodigo) {
		this.prvCodigo = prvCodigo;
	}
	public String getPrvNombre() {
		return prvNombre;
	}
	public void setPrvNombre(String prvNombre) {
		this.prvNombre = prvNombre;
	}

    



}
