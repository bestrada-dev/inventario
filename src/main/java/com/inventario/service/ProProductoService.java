package com.inventario.service;
import com.inventario.model.ProProducto;


import java.util.List;

import org.springframework.stereotype.Service;

public interface ProProductoService {
    List<ProProducto> findAll();
    boolean crearProducto(ProProducto producto);
    boolean borrarProducto(String proCodigo);
}
