package com.inventario.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventario.model.TajTarjeta;
import com.inventario.service.TajTarjetaService;
import com.inventario.dao.impl.TajTarjetaDaoImpl;;

@Service
public class TajTarjetaServiceImpl implements TajTarjetaService{

	@Autowired
	private TajTarjetaDaoImpl tajTarjetaDaoImpl;
	
	@Override
	public List<TajTarjeta> findAll() {
		return tajTarjetaDaoImpl.findAll();
	}

}
