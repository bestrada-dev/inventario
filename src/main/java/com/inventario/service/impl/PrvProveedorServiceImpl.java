package com.inventario.service.impl;

import com.inventario.dao.*;
import com.inventario.model.*;
import com.inventario.service.*;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrvProveedorServiceImpl implements PrvProveedorService{

	@Autowired
	private PrvProveedorDao prvProveedorDao;
	
	@Override
	public List<PrvProveedor> findAll() {
		// TODO Auto-generated method stub
		return prvProveedorDao.findAll();
	}

}
