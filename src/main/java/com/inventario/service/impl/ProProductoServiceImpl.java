package com.inventario.service.impl;

import com.inventario.dao.ProProductoDao;

import com.inventario.model.*;
import com.inventario.service.*;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProProductoServiceImpl implements ProProductoService {

	@Autowired
	private ProProductoDao proProductoDao;
	
	
	public List<ProProducto> findAll() {
		// TODO Auto-generated method stub
		return proProductoDao.findAll();
	}

	@Override
	public boolean crearProducto(ProProducto producto) {
		// TODO Auto-generated method stub
		proProductoDao.crearProducto(producto);
 	   return true;
	}

	@Override
	public boolean borrarProducto(String proCodigo) {
		// TODO Auto-generated method stub
		proProductoDao.borrarProducto(proCodigo);
	 	   return true;
	}


}
