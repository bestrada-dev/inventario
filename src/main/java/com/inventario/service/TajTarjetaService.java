package com.inventario.service;

import java.util.List;

import com.inventario.model.TajTarjeta;

public interface TajTarjetaService {
	List<TajTarjeta> findAll();
}
